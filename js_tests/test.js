describe("DOM Tests", function () {
	describe('Check Go Puzzle Div', function(){
		var tsumegoDiv = document.getElementById('tsumego_id');
	  it("is in the DOM", function () {
	      expect(tsumegoDiv).to.not.equal(null);
	  });

	  it("has the right text", function () {
	      expect(tsumegoDiv.innerText).to.equal("Your browser doesn't support WGo Player. Use some modern browser like IE9, Google Chrome or Mozilla Firefox.");
	  });
	});

	describe('Check Continue Button', function(){
		var tut_1_button = document.getElementById('tut_1_button');
	  it("is in the DOM", function () {
	      expect(tut_1_button).to.not.equal(null);
	  });

		it("has the text Continue", function(){
			expect(tut_1_button.innerText).to.equal("Continue");
		})

	  it("is invisible", function () {
	      expect(tut_1_button.style.display).to.equal('none');
	  });
	});

	describe('Check Finish Button', function(){
		var tut_1_finish = document.getElementById('tut_1_finish');
	  it("is in the DOM", function () {
	      expect(tut_1_finish).to.not.equal(null);
	  });

		it("has the text Move to Next Level", function(){
			expect(tut_1_finish.innerText).to.equal("Move to next level!");
		})

	  it("is invisible", function () {
	      expect(tut_1_finish.style.display).to.equal('none');
	  });
	});

	describe('Check Tutorial Text', function(){
		var tut_1_text = document.getElementById('tut_1_text');
	  it("is in the DOM", function () {
	      expect(tut_1_finish).to.not.equal(null);
	  });

		it("has no text", function(){
			expect(tut_1_text.innerText).to.equal("");
		})

	  it("is under dialog div", function () {
	      expect(tut_1_text.parentElement.className).to.equal('dialog');
	  });
	});

});

describe('Test Tutorial 1 Javascript', function() {
	describe('Check continue function', function() {
		it('should return false when text is empty', function() {
			assert.equal(false, checkContinue(""));
		});

		it('should return false when text is a character', function() {
			assert.equal(false, checkContinue("a"));
		});

		it('should return true when text is Correct', function() {
			assert.equal(true, checkContinue("Correct"));
		});

		it('should return true when text is Correct!', function() {
			assert.equal(true, checkContinue("Correct!"));
		});

		it('should return true when text is correct', function() {
			assert.equal(true, checkContinue("correct"));
		});

		it('should return true when text is correct', function() {
			assert.equal(true, checkContinue("correct"));
		});

		it('should return true when text is correct', function() {
			assert.equal(true, checkContinue("correct"));
		});

		it('should return true when text is Example', function() {
			assert.equal(true, checkContinue("Example"));
		});

		it('should return true when text is Example', function() {
			assert.equal(true, checkContinue("Example"));
		});

		it('should return true when text is example', function() {
			assert.equal(true, checkContinue("example"));
		});

		it('should return true when text is This is Correct', function() {
			assert.equal(true, checkContinue("This is Correct"));
		});

		it('should return true when text is An example', function() {
			assert.equal(true, checkContinue("An example"));
		});

		it('should return true when text is Correct Example', function() {
			assert.equal(true, checkContinue("Correct Example"));
		});

		it('should return false when text is Incorrect', function() {
			assert.equal(false, checkContinue("Incorrect"));
		});

	});
});
