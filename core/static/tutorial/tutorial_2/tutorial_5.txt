I cannot emphasize the liberty count enough! Always keep track of your
soldier's as well as your enemy's liberties!
<br>
The enemies are almost dead! We want to save as much of our resources as
possible.
<br>
Send as few soldiers as possible to finish the enemies off!
<br>
<br>
But with death, we also have to discuss life!
