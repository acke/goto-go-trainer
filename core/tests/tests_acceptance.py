"""
File for Acceptance test
"""
from urllib.request import urlopen
from bs4 import BeautifulSoup
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.test import Client
from core.models import JourneyNodes, GoUser

# Create your tests here.
# Assumes that default test is used
class AcceptanceTest(TestCase):
    """
    Acceptance Test class
    """
    def setUp(self):
        # Open the url and pass to BeautifulSoup
        html = urlopen("https://goto-go-trainer.herokuapp.com/")
        self.soup = BeautifulSoup(html, "html.parser")
        # items for the navigation tests
        # Fill with list of all navBar values, including navSubBar
        self.nav_vals = [' Home', 'Go Library']
        self.nav_subbar = []			# Fill with list of subBar Values

# Test title of the page
# _________________________

    def test_title(self):
        """
        Make sure the title is correct
        """
        self.assertEqual("Goto Go Trainer", self.soup.html.head.title.string)

# Test Navigation Bar
# ______________________

    def test_nav_bar(self):
        """
        Check that all navigation Bar values are correct
        """
        ulist = self.soup.html.div.ul
        for match in ulist.findAll('span'):
            match.unwrap()
        for match in ulist.findAll('li'):
            for li2 in match.findAll('li'):
                li2.unwrap()
        lst = ulist.findAll('li')
        for val in enumerate(lst):
            i = val[0]
            self.assertEqual(self.nav_vals[i], val[1].a.contents[0])

    def test_nav_subbar(self):
        """
        Verify that subBars have the correct values
        """
        count = 0
        ulist = self.soup.html.div.ul
        # remove item that are not being checked here
        for match in ulist.findAll('span'):
            match.unwrap()
        for match in ulist.findAll('li', {'class': "divider"}):
            match.unwrap()
        # search for sublits
        for match in ulist.findAll('li'):
            if match.find('ul'):
                subbar_list = match.findAll('li')
                self.assertEqual(len(self.nav_subbar[count]), len(subbar_list))
                for val in enumerate(subbar_list):
                    i = val[0]
                    self.assertEqual(self.nav_subbar[count][i],
                                     subbar_list[i].a.contents[0])
                count += 1

    def test_login(self):
        """
        Test login
        """
        username = 'testuser'
        password = 'testpass'
        user = get_user_model()
        user.objects.create_user(username, password=password)
        logged_in = self.client.login(username=username, password=password)
        self.assertTrue(logged_in)

    def test_login_nav(self):
        """
        Test navbar after login. This test is done locally
        """
	    #login
        cli = Client()
        username = 'testuser'
        password = 'testpass'
        user = get_user_model()
        user.objects.create_user(username, password=password)
        logged_in = cli.login(username=username, password=password)
        self.assertTrue(logged_in)
        response = cli.get('/')
        soup = BeautifulSoup(response.content, "html.parser")
        nav_vals = [' Home', ' Journey Map', ' Account ', ' Go Library']

        #navbar test
        ulist = soup.html.div.ul
        for match in ulist.findAll('span'):
            match.unwrap()
        for match in ulist.findAll('li'):
            for li2 in match.findAll('li'):
                li2.unwrap()
        lst = ulist.findAll('li')
        for val in enumerate(lst):
            i = val[0]
            self.assertEqual(nav_vals[i], val[1].a.contents[0])

    def test_login_subnav(self):
        """
        Test sub_navbar after login. This test is done locally
        """
        #login
        cli = Client()
        username = 'testuser'
        password = 'testpass'
        user = get_user_model()
        user.objects.create_user(username, password=password)
        logged_in = cli.login(username=username, password=password)
        self.assertTrue(logged_in)
        response = cli.get('/')
        soup = BeautifulSoup(response.content, "html.parser")
        nav_subbar = [['View Profile', 'Settings', 'Logout']]

        #subnavbar test
        count = 0
        ulist = soup.html.div.ul
        # remove item that are not being checked here
        for match in ulist.findAll('span'):
            match.unwrap()
        for match in ulist.findAll('li', {'class': "divider"}):
            match.unwrap()
        # search for sublits
        for match in ulist.findAll('li'):
            if match.find('ul'):
                subbar_list = match.findAll('li')
                self.assertEqual(len(nav_subbar[count]), len(subbar_list))
                for val in enumerate(subbar_list):
                    i = val[0]
                    self.assertEqual(nav_subbar[count][i],
                                     subbar_list[i].a.contents[0])
                count += 1

    def test_login_name(self):
        """
        Test that name appears after logging in
        """
        #login
        cli = Client()
        username = 'testuser'
        password = 'testpass'
        user = get_user_model()
        user.objects.create_user(username, password=password)
        logged_in = cli.login(username=username, password=password)
        self.assertTrue(logged_in)
        response = cli.get('/')
        soup = BeautifulSoup(response.content, "html.parser")
        plist = soup.findAll('p')
        self.assertTrue(username in plist[1].text)

    def test_profile_name(self):
        """
        Check to see if test values appear properly in profile page
        """
        cli = Client()
        username = 'testuser'
        password = 'testpass'
        user = get_user_model()
        user.objects.create_user(username, password=password)
        logged_in = cli.login(username=username, password=password)
        self.assertTrue(logged_in)
        response = cli.get('/account/profile/')
        soup = BeautifulSoup(response.content, "html.parser")
        h1_list = soup.findAll('h1')
        self.assertTrue(username in h1_list[0].text)

    def test_profile_levels(self):
        """
        Check to see if test values appear properly in profile page for a new user
        """
        cli = Client()
        username = 'testuser'
        password = 'testpass'
        user = get_user_model()
        user.objects.create_user(username, password=password)
        logged_in = cli.login(username=username, password=password)
        self.assertTrue(logged_in)
        response = cli.get('/account/profile/')
        soup = BeautifulSoup(response.content, "html.parser")
        h2_list = soup.findAll('h2')
        self.assertTrue(h2_list[0].text.endswith("0"))

    def test_profile_levels2(self):
        """
        Check to see if test values appear properly in profile page when user
        has completed some tutorials
        """
        cli = Client()
        username = 'testuser'
        password = 'testpass'
        user = get_user_model()
        user_vals = user.objects.create_user(username, password=password)
        logged_in = cli.login(username=username, password=password)
        node = JourneyNodes.objects.create(name="tnode", region="tregion", link="t-link")
        gouser = GoUser.objects.create(user_id=user_vals, last_completed_node=node)
        gouser.last_completed_node = JourneyNodes.objects.get(pk=1)
        gouser.save()
        self.assertTrue(logged_in)
        response = cli.get('/account/profile/')
        soup = BeautifulSoup(response.content, "html.parser")
        h2_list = soup.findAll('h2')
        self.assertTrue(h2_list[0].text.endswith("1"))
